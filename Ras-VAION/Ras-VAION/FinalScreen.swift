//
//  FinalScreen.swift
//  Ras-VAION
//
//  Created by Rastislav Smolen on 04/03/2020.
//  Copyright © 2020 Rastislav Smolen. All rights reserved.
//

import Foundation
import UIKit

class FinalScreen : UIViewController
{
    
var finalSentData : String?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        guard let data = finalSentData else {return}
        print("\(data)")
        // Do any additional setup after loading the view.
    }
}
