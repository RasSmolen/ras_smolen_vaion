//
//  ViewController.swift
//  Ras-VAION
//
//  Created by Rastislav Smolen on 03/03/2020.
//  Copyright © 2020 Rastislav Smolen. All rights reserved.
//
import Foundation
import UIKit

class ViewController: UIViewController

    
{
    var model = Networking()
    
    
    @IBOutlet var userNameTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var IpAdress: UITextField!
    
    @IBOutlet var Connect: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        passwordTextField.isHidden = true
        userNameTextField.isHidden = true
        // Do any additional setup after loading the view.
    }

 
    func execute()
    {
        guard let ipAddressToSearch = IpAdress.text, let password = passwordTextField.text, let username = userNameTextField.text else {return}
        model.connectToServer(ipAddress: ipAddressToSearch, credentials: Credentials.init(username: username, password: password)) { (NetworkingResponse) in if NetworkingResponse.code == 200
        {
            self.passwordTextField.isHidden = true
            self.userNameTextField.isHidden = true
            self.successAlert()


           
        }else if NetworkingResponse.code == 401
        {
        
            self.passwordTextField.isHidden = false
            self.userNameTextField.isHidden = false
            
            print(NetworkingResponse)
            self.passwordAlert()

        }else
            {
                self.enterValidIpAddressAlert()
            }

            
        }
    }
    func enterValidIpAddressAlert()
        {
    
            DispatchQueue.main.async {[weak self] in
                
                let passwordRequiredAlert = UIAlertController(title: "Enter Valid IP Adress", message:
                        "", preferredStyle: .alert)
                    passwordRequiredAlert.addAction(UIAlertAction(title: "Ok", style: .default))
    
                self?.present(passwordRequiredAlert, animated: true, completion: nil)
    
            }
            
    }
    func pass ()
    {
        self.performSegue(withIdentifier: "success_", sender: nil)

    }
    func successAlert()
        {

            DispatchQueue.main.async {[weak self] in
                let passwordRequiredAlert = UIAlertController(title: "Succces", message:
                    "", preferredStyle: .alert)
                passwordRequiredAlert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: {action in self?.pass()}))
    
                self?.present(passwordRequiredAlert, animated: true, completion: nil)
    
            }
            
    }
            
    func passwordAlert()
        {
    
            DispatchQueue.main.async {[weak self] in
                
                let passwordRequiredAlert = UIAlertController(title: " Fill in correct Password", message:
                        "and user Name", preferredStyle: .alert)
                    passwordRequiredAlert.addAction(UIAlertAction(title: "Ok", style: .default))
    
                self?.present(passwordRequiredAlert, animated: true, completion: nil)
    
            }
            
    }
    @IBAction func connectToServer(_ sender: UIButton)
    {
        execute()
        passwordTextField.text = ""
        userNameTextField.text = ""
    }

}

